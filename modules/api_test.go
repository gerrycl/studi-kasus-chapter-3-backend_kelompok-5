package modules

import (
	"bytes"
	"net/http"
	"net/http/httptest"
	"testing"
	"x/database"

	"github.com/gorilla/mux"
)

func TestRegisterUser(t *testing.T) {
	database.InitDB()
	database.InitMongoClient()

	jsonData := `{"name":"John Doe","address":"123 Main St","email":"johndoe@example.com"}`
	req, err := http.NewRequest("POST", "/register", bytes.NewBufferString(jsonData))
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Set("Content-Type", "application/json")

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(RegisterUser)

	router := mux.NewRouter()
	router.Handle("/register", handler)

	router.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusCreated {
		t.Errorf("Handler returned wrong status code: got %v, want %v", status, http.StatusCreated)
	}

	expectedResponse := "User registered successfully\n"
	if rr.Body.String() != expectedResponse {
		t.Errorf("Handler returned unexpected body: got %v, want %v", rr.Body.String(), expectedResponse)
	}
}

func TestGetUserFromMySQL(t *testing.T) {
	database.InitDB()

	req, err := http.NewRequest("GET", "/user/mysql/a6194c1d-dab8-417b-a7c9-464f8a5bd199", nil)
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(GetUserFromMySQL)

	router := mux.NewRouter()
	router.Handle("/user/mysql/{userId}", handler)

	router.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusOK {
		t.Errorf("Handler returned wrong status code: got %v, want %v", status, http.StatusOK)
	}
}

func TestGetUserFromMongoDB(t *testing.T) {
	database.InitMongoClient()

	req, err := http.NewRequest("GET", "/user/mongo/a6194c1d-dab8-417b-a7c9-464f8a5bd199", nil)
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(GetUserFromMongoDB)

	router := mux.NewRouter()
	router.Handle("/user/mongo/{userId}", handler)

	router.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusOK {
		t.Errorf("Handler returned wrong status code: got %v, want %v", status, http.StatusOK)
	}
}

func TestInsertAccount(t *testing.T) {
	database.InitDB()
	database.InitMongoClient()

	jsonData := `{"msisdnCustomer":"087748172312","userId":"41fe7300-93d6-44ae-bd2b-88796960154d"}`
	req, err := http.NewRequest("POST", "/account/insertAccount", bytes.NewBufferString(jsonData))
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Set("Content-Type", "application/json")

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(InsertAccount)

	router := mux.NewRouter()
	router.Handle("/account/insertAccount", handler)

	router.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusCreated {
		t.Errorf("Handler returned wrong status code: got %v, want %v", status, http.StatusCreated)
	}

	expectedResponse := "Account inserted successfully\n"
	if rr.Body.String() != expectedResponse {
		t.Errorf("Handler returned unexpected body: got %v, want %v", rr.Body.String(), expectedResponse)
	}
}

func TestGetUserFromAccountID(t *testing.T) {
	database.InitDB()

	req, err := http.NewRequest("GET", "/account/081245892048", nil)
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(GetUsersByAccountID)

	router := mux.NewRouter()
	router.Handle("/account/{accountID}", handler)

	router.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusOK {
		t.Errorf("Handler returned wrong status code: got %v, want %v", status, http.StatusOK)
	}
}
