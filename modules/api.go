package modules

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"x/database"
	"x/models"

	"github.com/google/uuid"
	"github.com/gorilla/mux"
	"go.mongodb.org/mongo-driver/bson"
)

func RegisterUser(w http.ResponseWriter, r *http.Request) {
	var newUser models.User
	err := json.NewDecoder(r.Body).Decode(&newUser)
	if err != nil {
		http.Error(w, "Invalid JSON data", http.StatusBadRequest)
		return
	}

	newUser.UserId = uuid.New().String()

	_, err = database.DB.Exec("INSERT INTO users (user_id, name, address, email) VALUES (?, ?, ?, ?)",
		newUser.UserId, newUser.Name, newUser.Address, newUser.Email)
	if err != nil {
		http.Error(w, "Failed to insert user into MySQL", http.StatusInternalServerError)
		return
	}

	collection := database.Client.Database("dbt").Collection("users")
	_, err = collection.InsertOne(context.Background(), newUser)
	if err != nil {
		http.Error(w, "Failed to insert user into MongoDB", http.StatusInternalServerError)
		log.Fatal(err)
		return
	}

	w.WriteHeader(http.StatusCreated)
	fmt.Fprintln(w, "User registered successfully")
}

func GetUserFromMySQL(w http.ResponseWriter, r *http.Request) {
	userID := mux.Vars(r)["userId"]

	row := database.DB.QueryRow("SELECT user_id, name, address, email FROM users WHERE user_id = ?", userID)

	var user models.User
	err := row.Scan(&user.UserId, &user.Name, &user.Address, &user.Email)
	if err != nil {
		http.Error(w, "User not found in MySQL", http.StatusNotFound)
		return
	}

	userJSON, err := json.Marshal(user)
	if err != nil {
		http.Error(w, "Error marshaling JSON", http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(userJSON)
}

func GetUserFromMongoDB(w http.ResponseWriter, r *http.Request) {
	userID := mux.Vars(r)["userId"]

	// Get the MongoDB collection.
	collection := database.Client.Database("dbt").Collection("users")

	filter := bson.M{"userid": userID}
	var user models.User
	err := collection.FindOne(context.Background(), filter).Decode(&user)
	if err != nil {
		http.Error(w, "User not found in MongoDB", http.StatusNotFound)
		return
	}

	userJSON, err := json.Marshal(user)
	if err != nil {
		http.Error(w, "Error marshaling JSON", http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(userJSON)
}

func InsertAccount(w http.ResponseWriter, r *http.Request) {
	var newAccount models.Account
	err := json.NewDecoder(r.Body).Decode(&newAccount)
	if err != nil {
		http.Error(w, "Invalid JSON data", http.StatusBadRequest)
		return
	}

	newAccount.AccountId = uuid.New().String()

	countQuery := "SELECT COUNT(*) FROM accounts WHERE msisdnCustomer = ?"
	var count int
	err = database.DB.QueryRow(countQuery, newAccount.MsisdnCustomer).Scan(&count)
	if err != nil {
		http.Error(w, "Error checking account count", http.StatusInternalServerError)
		return
	}
	if count >= 3 {
		http.Error(w, "MsisdnCustomer cannot hold more than 3 userIDs", http.StatusForbidden)
		return
	}

	checkQuery := "SELECT COUNT(*) FROM accounts WHERE user_id = ?"
	err = database.DB.QueryRow(checkQuery, newAccount.UserId).Scan(&count)
	if err != nil {
		http.Error(w, "Error checking userID association", http.StatusInternalServerError)
		return
	}
	if count > 0 {
		http.Error(w, "userID is already associated with an account", http.StatusForbidden)
		return
	}

	userExistsQuery := "SELECT COUNT(*) FROM users WHERE user_id = ?"
	err = database.DB.QueryRow(userExistsQuery, newAccount.UserId).Scan(&count)
	if err != nil {
		http.Error(w, "Error checking if userID exists", http.StatusInternalServerError)
		return
	}
	if count == 0 {
		http.Error(w, "userID does not exist", http.StatusNotFound)
		return
	}

	_, err = database.DB.Exec("INSERT INTO accounts (msisdnCustomer, user_id) VALUES (?, ?)",
		newAccount.MsisdnCustomer, newAccount.UserId)
	if err != nil {
		http.Error(w, "Failed to insert account into MySQL", http.StatusInternalServerError)
		return
	}

	collection := database.Client.Database("dbt").Collection("accounts")
	_, err = collection.InsertOne(context.Background(), newAccount)
	if err != nil {
		http.Error(w, "Failed to insert account into MongoDB", http.StatusInternalServerError)
		log.Fatal(err)
		return
	}

	w.WriteHeader(http.StatusCreated)
	fmt.Fprintln(w, "Account inserted successfully")
}

func GetUsersByAccountID(w http.ResponseWriter, r *http.Request) {
	accountID := mux.Vars(r)["accountID"]

	query := `
		SELECT u.user_id, u.name, u.address, u.email
		FROM users u
		WHERE u.user_id IN (SELECT a.user_id FROM accounts a WHERE a.msisdnCustomer = ?)
	`

	rows, err := database.DB.Query(query, accountID)
	if err != nil {
		http.Error(w, "Error querying database", http.StatusInternalServerError)
		return
	}
	defer rows.Close()

	var users []models.User
	for rows.Next() {
		var user models.User
		err := rows.Scan(&user.UserId, &user.Name, &user.Address, &user.Email)
		if err != nil {
			http.Error(w, "Error scanning database rows", http.StatusInternalServerError)
			return
		}
		users = append(users, user)
	}

	if len(users) == 0 {
		http.Error(w, "No users found for the given account", http.StatusNotFound)
		return
	}

	usersJSON, err := json.Marshal(users)
	if err != nil {
		http.Error(w, "Error marshaling JSON", http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(usersJSON)
}
