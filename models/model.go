package models

type User struct {
	UserId  string `json:"userId"`
	Name    string `json:"name"`
	Address string `json:"address"`
	Email   string `json:"email"`
}

type Account struct {
	AccountId      string `json:"accountId"`
	MsisdnCustomer string `json:"msisdnCustomer"`
	UserId         string `json:"userId"`
}
