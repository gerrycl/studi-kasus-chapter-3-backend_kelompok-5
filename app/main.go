package main

import (
	"log"
	"net/http"
	"x/database"
	"x/modules"

	"github.com/gorilla/mux"
)

func main() {
	// Initialize database connections.
	database.InitDB()
	database.InitMongoClient()

	// Create a new router using Gorilla Mux.
	router := mux.NewRouter()

	// Define routes.
	router.HandleFunc("/user/register", modules.RegisterUser).Methods("POST")
	router.HandleFunc("/user/mysql/{userId}", modules.GetUserFromMySQL).Methods("GET")
	router.HandleFunc("/user/mongo/{userId}", modules.GetUserFromMongoDB).Methods("GET")
	router.HandleFunc("/account/insertAccount", modules.InsertAccount).Methods("POST")
	router.HandleFunc("/account/{accountID}", modules.GetUsersByAccountID).Methods("GET")

	// Run the server.
	http.Handle("/", router)
	log.Fatal(http.ListenAndServe(":8080", nil))
}
